package sdkcash.findcash;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Bhaskar on 8/14/17.
 */

public class Matched_Request_Info extends AppCompatActivity {

    private String name;
    private String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.matched_request_info);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                name = null;
                email = null;
            } else {
                name = extras.getString("name");
                email = extras.getString("email");
            }
        }

        TextView textViewName = (TextView)findViewById(R.id.name);
        TextView textViewEmail=(TextView)findViewById(R.id.email);

        textViewName.setText(name);
        textViewEmail.setText(email);

    }
}