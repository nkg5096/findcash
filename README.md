## Findcash
Findcash is an application that helps travelers easily exchange currency. Currently there are two main branches of development: web and mobile.

### Mobile
Mobile development is being built using Android studio. This means that the main programming language will be Java.

Some resources to help you learn Java:

[CodeAcademy (Java)](https://www.codecademy.com/learn/learn-java)

### Web
Web development is being built using Node.js and Express. This means that the main programming language is Javascript, but also means that knowing HTML and CSS  will help with static webpages.

Some resources to help you learn Javascript:

[CodeAcademy (Javascript)](https://www.codecademy.com/learn/learn-javascript)

Some resources to help you learn HTML and CSS:

[CodeAcademy (HTML)](https://www.codecademy.com/learn/learn-html)

[CodeAcademy (CSS)](https://www.codecademy.com/learn/learn-css)

[learn.shayhowe.com (HTML & CSS)](https://learn.shayhowe.com/html-css/building-your-first-web-page/)

### Updates
Resource lists will be updated as we find better resources. If you find something that helped you not included on this list, please let us know!